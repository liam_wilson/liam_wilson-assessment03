﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment03
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            names.ItemsSource = LoadFNames();
        }

        static List<string> LoadFNames()
        {
            var command = $"Select FNAME from tbl_people";
            var list = MySQLCustom.ShowInList(command);
            return list;
        }

        void SelectedPerson(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var a = MySQLCustom.ShowInList(command);

            id.Text = a[0];
            selectedfname.Text = a[1];
            selectedlname.Text = a[2];
            selecteddob.Text = a[3];
            selectedstr_number.Text = a[4];
            selectedstr_name.Text = a[5];
            selectedpostcode.Text = a[6];
            selectedcity.Text = a[7];
            selectedphone1.Text = a[8];
            selectedphone2.Text = a[9];
            selectedemail.Text = a[10];

        }

        void ClearAll()
        {
            fname.Text = "";
            lname.Text = "";
            dob.Text = "";
            str_number.Text = "";
            str_name.Text = "";
            postcode.Text = "";
            city.Text = "";
            phone1.Text = "";
            phone2.Text = "";
            email.Text = "";
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = !Splitter.IsPaneOpen;
        }
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            rootPivot.SelectedItem = pivot1pane;
        }

        private async void SubmitAllButton_Click(object sender, RoutedEventArgs e)
        {
            if (fname.Text == "" || lname.Text == "" || dob.Text == "" || str_number.Text == "" || str_name.Text == "" || postcode.Text == "" || city.Text == "" || phone1.Text == "" || phone2.Text == "" || email.Text == "")
            {
                string message;
                message = "You have not input any information, please go back and do so to continue";
                var dialog = new MessageDialog(message);
                dialog.Title = "Please Input Info";

                dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.AddData(fname.Text, lname.Text, dob.Text, str_number.Text, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
                names.ItemsSource = LoadFNames();
                ClearAll();
            }
            
        }

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rootPivot.SelectedItem = pivot5pane;
            if (names.SelectedItem != null)
            {
                SelectedPerson(names.SelectedItem.ToString());
            }
        }

        private async void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedfname.Text == "" || selectedlname.Text == "" || selecteddob.Text == "" || selectedstr_number.Text == "" || selectedstr_name.Text == "" || selectedpostcode.Text == "" || selectedcity.Text == "" || selectedphone1.Text == "" || selectedphone2.Text == "" || selectedemail.Text == "")
            {
                string message;
                message = "You have not selected a user to update, please go back and do so to continue";
                var dialog = new MessageDialog(message);
                dialog.Title = "Please Select User";

                dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.UpdateData(selectedfname.Text, selectedlname.Text, selecteddob.Text, selectedstr_number.Text, selectedstr_name.Text, selectedpostcode.Text, selectedcity.Text, selectedphone1.Text, selectedphone2.Text, selectedemail.Text, id.Text);
                names.ItemsSource = LoadFNames();
                ClearAll();
            }
        }

        private async void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedfname.Text == "" || selectedlname.Text == "" || selecteddob.Text == "" || selectedstr_number.Text == "" || selectedstr_name.Text == "" || selectedpostcode.Text == "" || selectedcity.Text == "" || selectedphone1.Text == "" || selectedphone2.Text == "" || selectedemail.Text == "")
            {
                string message;
                message = "You have not selected a user to delete, please go back and do so to continue";
                var dialog = new MessageDialog(message);
                dialog.Title = "Please Select User";

                dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
                var res = await dialog.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteData(id.Text);
                names.ItemsSource = LoadFNames();
                ClearAll();
            }
        }
    }
}
